package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"strings"

	"gitlab.com/divilla/sitemap/pkg/xlog"

	"gitlab.com/divilla/sitemap/internal/link"
	"gitlab.com/divilla/sitemap/internal/worker"
	"gitlab.com/divilla/sitemap/pkg/sitemap_writer"
	"gitlab.com/divilla/sitemap/pkg/xhttp"
)

func main() {
	outputDirFlag := flag.String("output-dir", "output", "directory to generate sitemaps into")
	logsDirFlag := flag.String("logs-dir", "logs", "log files directory")
	parallelFlag := flag.Int("parallel", 16, "number of concurrent requests")
	maxDepthFlag := flag.Int("max-depth", 3, "number of subsequent urls to follow")
	maxRetryFlag := flag.Int("max-retry", 3, "number of retries in case of failure")
	flag.Parse()

	baseUrl := validateCommand(flag.Arg(0))

	// implements graceful shutdown
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)

	l := xlog.New(*logsDirFlag)
	ca, err := xhttp.NewClientAdapter()
	if err != nil {
		log.Fatalln(err)
	}

	//  test if it works on single core
	//	runtime.GOMAXPROCS(1)

	lq := link.NewLinkQueue(baseUrl, *maxDepthFlag, *maxRetryFlag)
	sw := sitemap_writer.New(l, *outputDirFlag, "sitemap.xml", "http://www.sitemaps.org/schemas/sitemap/0.9")
	mgr := worker.NewProcessor(l, ca, lq, sw)

	go gracefulShutdown(sigChan, mgr)

	mgr.Start(int64(*parallelFlag))
}

func gracefulShutdown(sigChan chan os.Signal, mgr *worker.Processor) {
	<-sigChan
	mgr.Cancel()
}

func validateCommand(address string) *url.URL {
	if address == "" {
		exe := filepath.Base(os.Args[0])
		fmt.Print("Sitemap is a console tool for building sitemap.xml files.\r\n\r\n")
		fmt.Printf("Usage: ./%s [OPTIONS]... [ADDRESS]\r\n\r\n", exe)
		fmt.Printf("Basic example: ./%s http://example.com\r\n\r\n", exe)
		fmt.Printf("Get help for options: ./%s --help\r\n\r\n", exe)
		os.Exit(1)
	}
	if !strings.HasPrefix(address, "http") {
		fmt.Println("Url scheme is required, address must start with: 'http://' or 'https://'")
		os.Exit(1)
	}
	baseUrl, err := url.Parse(address)
	if err != nil {
		fmt.Printf("Invalid url: '%s' with error: %v\r\n", address, err)
		os.Exit(1)
	}
	if baseUrl.Hostname() == "" {
		fmt.Printf("Cannot resolve hostname: '%s'", address)
		os.Exit(1)
	}

	return baseUrl
}
