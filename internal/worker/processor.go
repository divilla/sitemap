package worker

import (
	"context"
	"sync/atomic"
	"time"

	"gitlab.com/divilla/sitemap/pkg/xlog"

	"gitlab.com/divilla/sitemap/internal/link"
	"gitlab.com/divilla/sitemap/internal/scraper"
	"gitlab.com/divilla/sitemap/pkg/sitemap_writer"
	"gitlab.com/divilla/sitemap/pkg/xhttp"
)

type (
	Processor struct {
		logger        *xlog.Processor
		client        *xhttp.ClientAdapter
		queue         *link.Queue
		writer        *sitemap_writer.Writer
		cancel        context.CancelFunc
		activeWorkers int64
	}
)

func NewProcessor(l *xlog.Processor, ca *xhttp.ClientAdapter, lq *link.Queue, sw *sitemap_writer.Writer) *Processor {
	return &Processor{
		logger: l,
		client: ca,
		queue:  lq,
		writer: sw,
	}
}

func (p *Processor) Start(maxConcurrent int64) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	p.cancel = cancel

STOP:
	for {
		select {
		case <-ctx.Done():
			break STOP
		default:
		}

		activeWorkers := atomic.LoadInt64(&p.activeWorkers)
		if activeWorkers == 0 && p.queue.IsEOQ() {
			break
		}
		if activeWorkers >= maxConcurrent {
			continue
		}
		if lnk := p.queue.Next(); lnk == nil {
			continue
		} else {
			atomic.AddInt64(&p.activeWorkers, 1)
			childCtx, childCancel := context.WithCancel(ctx)
			go p.Process(childCtx, lnk, childCancel)
		}
	}

	p.writer.Write()
}

func (p *Processor) Process(ctx context.Context, link *link.Link, cancel context.CancelFunc) {
	defer atomic.AddInt64(&p.activeWorkers, -1)
	defer cancel()

	select {
	case <-ctx.Done():
		return
	default:
	}

	p.logger.Task(link.Id(), link.Depth(), link.Retry(), link.Address())
	if link.Retry() > 0 {
		time.Sleep(time.Duration(link.Retry()) * time.Second)
	}

	address := link.Address()
	res, err := p.client.GetWithContext(ctx, address)
	if err != nil {
		p.logger.Failed(link.Id(), link.Retry(), link.Address(), err)
		p.queue.PushRetryBack(link)
		return
	}

	p.writer.AddUrl(res.Url, res.LastModified, link.Id())

	select {
	case <-ctx.Done():
		return
	default:
	}

	if !link.IsMaxDepth() {
		scrapedUrls := scraper.AHrefs(res.Url, res.Body)
		filteredUrls := scraper.Filter(p.queue.MasterDomain(), res.Url, scrapedUrls)
		urls := scraper.Parse(res.Url, filteredUrls)
		p.queue.PushUrlsBack(urls, link.Depth()+1)
	}
}

func (p *Processor) Cancel() {
	p.cancel()
}
