package link

type (
	Link struct {
		id         int
		address    string
		depth      int
		isMaxDepth bool
		retry      int
		error      error
	}
)

func (l *Link) Id() int {
	return l.id
}

func (l *Link) Address() string {
	return l.address
}

func (l *Link) IsFirst() bool {
	return l.depth == 0
}

func (l *Link) Depth() int {
	return l.depth
}

func (l *Link) IsMaxDepth() bool {
	return l.isMaxDepth
}

func (l *Link) Retry() int {
	return l.retry
}

func (l *Link) GetError() error {
	return l.error
}

func (l *Link) SetError(err error) {
	l.error = err
}
