package link

import (
	"net/url"
	"runtime"
	"strings"
	"sync/atomic"
)

const (
	open   = 0
	closed = 1
)

type (
	Queue struct {
		linkId       int
		maxDepth     int
		maxRetry     int
		baseUrl      *url.URL
		masterDomain string
		queue        []*Link
		set          map[string]struct{}
		isEOQ        bool
		state        int64
		next         int64
	}
)

func NewLinkQueue(baseUrl *url.URL, maxDepth, maxRetry int) *Queue {
	subdomains := strings.Split(baseUrl.Hostname(), ".")
	masterDomain := strings.Join(subdomains[len(subdomains)-2:], ".")

	targetUrl := baseUrl.String()
	return &Queue{
		maxDepth:     maxDepth,
		maxRetry:     maxRetry,
		baseUrl:      baseUrl,
		masterDomain: masterDomain,
		queue: []*Link{{
			address:    targetUrl,
			isMaxDepth: maxDepth == 0,
		}},
		set:  map[string]struct{}{targetUrl: {}},
		next: -1,
	}
}

func (q *Queue) NewLink(address string, depth int) *Link {
	q.linkId++

	return &Link{
		id:         q.linkId,
		address:    address,
		depth:      depth,
		isMaxDepth: depth == q.maxDepth,
	}
}

func (q *Queue) PushUrlsBack(urls []*url.URL, depth int) {
	for !atomic.CompareAndSwapInt64(&q.state, open, closed) {
		runtime.Gosched()
	}
	defer atomic.StoreInt64(&q.state, open)

	for _, u := range urls {
		address := u.String()
		if _, ok := q.set[address]; ok {
			continue
		}
		q.set[address] = struct{}{}
		q.queue = append(q.queue, q.NewLink(address, depth))
		if q.isEOQ {
			q.isEOQ = false
		}
	}
}

func (q *Queue) PushRetryBack(u *Link) bool {
	for !atomic.CompareAndSwapInt64(&q.state, open, closed) {
		runtime.Gosched()
	}
	defer atomic.StoreInt64(&q.state, open)

	u.retry++
	if u.retry >= q.maxRetry {
		return false
	}

	q.queue = append(q.queue, u)
	q.isEOQ = false
	return true
}

func (q *Queue) Next() *Link {
	if q.isEOQ {
		return nil
	}

	for !atomic.CompareAndSwapInt64(&q.state, open, closed) {
		runtime.Gosched()
	}
	defer atomic.StoreInt64(&q.state, open)

	if int(q.next+1) >= len(q.queue) {
		q.isEOQ = true
		return nil
	}

	return q.queue[atomic.AddInt64(&q.next, 1)]
}

func (q *Queue) MasterDomain() string {
	return q.masterDomain
}

func (q *Queue) IsEOQ() bool {
	return q.isEOQ
}

//
//func (q *Queue) BaseUrl() *url.URL {
//	return q.baseUrl
//}
//
//func (q *Queue) Len() int {
//	return len(q.queue)
//}
