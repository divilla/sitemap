package scraper

//
// Set of pure functions used to scrape urls from html,
// filter only urls that match target hostname and
// parse relative urls
//
// Unnecessary triple iteration and use of pure functions
// are implemented to increase code readability
// and make testing more transparent
//
// Following Martin Fowler's guidelines on:
// - nesting conditionals
// - adding result function
// - ...
//
// *** see:https://martinfowler.com/articles/refactoring-adaptive-model.html
//
