package scraper

import (
	"net/url"
	"strings"
)

func MasterDomain(ref *url.URL) string {
	subdomains := strings.Split(ref.Hostname(), ".")
	return strings.Join(subdomains[len(subdomains)-2:], ".")
}

func HasMasterDomain(masterDomain string, ref *url.URL) bool {
	return strings.HasSuffix(ref.Hostname(), masterDomain)
}
