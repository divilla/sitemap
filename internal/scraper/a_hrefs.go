package scraper

import (
	"fmt"
	"log"
	"net/url"
	"regexp"
	"strings"
)

var (
	// I was playing with different <a> tag variants on MDN,
	// browsers can "swallow" all sorts of stuff, so in production world
	// this task would require more attention
	//aTagRegexp = regexp.MustCompile(`(?s)<a[^\S]+(<)|[^<]+?>`)
	aTagRegexp = regexp.MustCompile(`(?s)<a[^\S]+[^<]+?>`)

	// I'm very well aware this must be achievable with single regex,
	// but considering the time I have on disposal to make this work
	// I'm choosing quality over performance, leaving fine-tuning open
	hrefAttrRegexp = regexp.MustCompile(`(?s)[^\S]+?href[^\S]*=[^\S]*"[^\S]*(.+?)[^\S]*?"`)
)

// AHrefs scrapes all valid href attributes from all <a> tags
func AHrefs(targetUrl *url.URL, htmlBytes []byte) []*url.URL {
	res := make([]*url.URL, 0)
	for _, aTag := range aTagRegexp.FindAll(htmlBytes, -1) {
		refs := hrefAttrRegexp.FindSubmatch(aTag)
		if len(refs) < 2 {
			continue
		}

		ref := string(refs[1])
		if strings.HasPrefix(ref, "mailto") {
			continue
		}
		if len(refs) > 1 && ref != "" {
			u, err := url.Parse(ref)
			if err != nil {
				log.Println(fmt.Errorf("invalid url scraped: %s, from: %s,  with error: %w", ref, targetUrl.String(), err))
				continue
			}

			res = append(res, u)
		}
	}

	return res
}
