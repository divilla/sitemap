package scraper

import (
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func TestParse(t *testing.T) {
	target := "https://www.example.com/aaa?bbb=1#ccc"
	targetUrl, err := url.Parse(target)
	assert.NoError(t, err)

	tests := []struct {
		input    string
		expected string
	}{
		{
			"http://www.example.com/aaa?bbb=1",
			"https://www.example.com/aaa?bbb=1",
		},
		{
			"http://example.com/aaa?bbb=1",
			"http://example.com/aaa?bbb=1",
		},
		{
			"/ddd?eee=1#fff",
			"https://www.example.com/ddd?eee=1#fff",
		},
		{
			"ddd?eee=1#fff",
			"https://www.example.com/ddd?eee=1#fff",
		},
	}

	var inputUrls []*url.URL
	var expectedUrs []string
	for _, test := range tests {
		inputUrl, err := url.Parse(test.input)
		assert.NoError(t, err)
		inputUrls = append(inputUrls, inputUrl)
		expectedUrs = append(expectedUrs, test.expected)
	}

	parsedUrls := Parse(targetUrl, inputUrls)
	strParsedUrls := mapUrlsToStrings(parsedUrls)
	assert.Equal(t, expectedUrs, strParsedUrls)
}
