package scraper

import (
	"net/url"
)

func Filter(masterDomain string, targetUrl *url.URL, urls []*url.URL) []*url.URL {
	var filtered []*url.URL
	targetHasMasterDomain := HasMasterDomain(masterDomain, targetUrl)
	for _, u := range urls {
		// If url is referencing different hostname ignore it
		if u.Hostname() != "" && HasMasterDomain(masterDomain, u) {
			filtered = append(filtered, u)
			continue
		}

		// If target was redirected to different domain
		// don't use relative urls
		if u.Hostname() == "" && targetHasMasterDomain {
			filtered = append(filtered, u)
		}

		// *** fragments: http://example.com/aaa#/bbb/ccc ***
		// As it is not clear from different Internet sources whether
		// sitemap.xml should contain fragments or not I'll ignore them
	}

	return filtered
}
