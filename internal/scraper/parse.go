package scraper

import (
	"net/url"
	"strings"
)

// Parse fixes relative paths & scheme to support http -> https redirect
func Parse(targetUrl *url.URL, urls []*url.URL) []*url.URL {
	res := make([]*url.URL, 0, len(urls))
	for _, u := range urls {
		if u.Hostname() == "" && !strings.HasPrefix(u.Path, "/") {
			u.Path = "/" + u.Path
		}

		// Avoid nesting conditionals to increase readability
		if u.Hostname() == "" {
			u.Host = targetUrl.Host
		}

		// Support http -> https redirect
		if targetUrl.Hostname() == u.Hostname() && u.Scheme != targetUrl.Scheme {
			u.Scheme = targetUrl.Scheme
		}

		res = append(res, u)
	}

	return res
}
