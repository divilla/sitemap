package scraper

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func mapUrlsToStrings(urls []*url.URL) []string {
	res := make([]string, len(urls))
	for k, v := range urls {
		res[k] = v.String()
	}
	return res
}

func mapStringsToUrls(t *testing.T, urls []string) []*url.URL {
	var res []*url.URL
	for _, v := range urls {
		u, err := url.Parse(v)
		assert.NoError(t, err, fmt.Errorf("failed to map string to urs: %s, with error: %w", v, err))
		res = append(res, u)
	}
	return res
}
