package scraper

import (
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

// ToDo more tests here
func TestScraper_Valid(t *testing.T) {
	input := []byte(`
<a href="https://example.com/aaa">

<a 
href
	=	"		https://example.com/bbb			
">

<a 
href
	=	"		


https://example.com/cxx@---a			


">

`)

	expected := []string{
		"https://example.com/aaa",
		"https://example.com/bbb",
		"https://example.com/cxx@---a",
	}

	targetUrl, err := url.Parse("https://example.com")
	assert.NoError(t, err)

	for k, v := range AHrefs(targetUrl, input) {
		assert.Equal(t, expected[k], v.String())
	}
}

// ToDo more tests here
func TestScraper_Invalid(t *testing.T) {
	input := []byte(`
<a
<aa href="https://example.com/aaa">
<a ahref="https://example.com/bbb">
<a href="https://example.com/ccc>
`)

	targetUrl, err := url.Parse("https://example.com")
	assert.NoError(t, err)

	assert.Equal(t, []*url.URL{}, AHrefs(targetUrl, input))
}

// ToDo more tests here
func TestScraper_Error(t *testing.T) {
	input := []byte(`
<a
<a href="cache_object:foo/bar">
<a href="()://()">
<a href="example!~!@#$%^&*().com/ccc">
`)

	targetUrl, err := url.Parse("https://example.com")
	assert.NoError(t, err)

	assert.Equal(t, []*url.URL{}, AHrefs(targetUrl, input))
}
