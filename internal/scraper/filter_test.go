package scraper

import (
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func TestFilter(t *testing.T) {
	testUrls := []string{
		"/",
		"http://example.com",
		"https://example.com",
		"http://www.example.com",
		"https://www.example.com",
		"https://example.com/aaa",
		"https://example.com/aaa?bbb=10",
		"https://example.com/aaa?bbb=10#ccc",
		"https://pero.com",
	}

	tests := []struct {
		masterDomain string
		targetUrl    string
		inputUrls    []string
		expectedUrls []string
	}{
		{
			masterDomain: "example.com",
			targetUrl:    "https://example.com",
			inputUrls:    testUrls,
			expectedUrls: []string{
				"/",
				"http://example.com",
				"https://example.com",
				"http://www.example.com",
				"https://www.example.com",
				"https://example.com/aaa",
				"https://example.com/aaa?bbb=10",
				"https://example.com/aaa?bbb=10#ccc",
			},
		},
		{
			masterDomain: "example.com",
			targetUrl:    "https://www.example.com",
			inputUrls:    testUrls,
			expectedUrls: []string{
				"/",
				"http://example.com",
				"https://example.com",
				"http://www.example.com",
				"https://www.example.com",
				"https://example.com/aaa",
				"https://example.com/aaa?bbb=10",
				"https://example.com/aaa?bbb=10#ccc",
			},
		},
		{
			masterDomain: "aaa.com",
			targetUrl:    "https://www.example.com",
			inputUrls:    testUrls,
			expectedUrls: []string{},
		},
	}

	for _, test := range tests {
		targetUrl, err := url.Parse(test.targetUrl)
		assert.NoError(t, err)

		inputUrls := mapStringsToUrls(t, test.inputUrls)
		resUrls := Filter(test.masterDomain, targetUrl, inputUrls)
		assert.Equal(t, test.expectedUrls, mapUrlsToStrings(resUrls))
	}
}
