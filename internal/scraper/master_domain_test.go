package scraper

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMasterDomain(t *testing.T) {
	strUrl := "https://www.example.com/aaa?bbb=1#ccc"
	ref, err := url.Parse(strUrl)
	assert.NoError(t, err)
	assert.Equal(t, "example.com", MasterDomain(ref))
	assert.True(t, HasMasterDomain("example.com", ref))
}
