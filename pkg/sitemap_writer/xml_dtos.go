package sitemap_writer

import "encoding/xml"

type (
	WriterUrlSet struct {
		XMLName xml.Name
		Urls    []WriterUrl `xml:"url"`
	}

	WriterUrl struct {
		Loc     string  `xml:"loc"`
		LastMod *string `xml:"lastmod"`
	}
)
