package sitemap_writer

import (
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"sync/atomic"
	"time"

	"gitlab.com/divilla/sitemap/pkg/xlog"
)

const (
	open   = 0
	closed = 1
)

type (
	Writer struct {
		logger   *xlog.Processor
		outDir   string
		filename string
		xmlns    string
		outSet   map[string]struct{}
		outMap   map[string][]WriterUrl
		outState int64
	}
)

func New(logger *xlog.Processor, outputDirectory, filename, xmlns string) *Writer {
	return &Writer{
		logger:   logger,
		outDir:   outputDirectory,
		filename: filename,
		xmlns:    xmlns,
		outSet:   make(map[string]struct{}),
		outMap:   make(map[string][]WriterUrl),
	}
}

func (w *Writer) AddUrl(ref *url.URL, lastModifiedHeader string, id int) {
	for !atomic.CompareAndSwapInt64(&w.outState, open, closed) {
		runtime.Gosched()
	}
	defer atomic.StoreInt64(&w.outState, open)

	address := ref.String()
	if _, ok := w.outSet[address]; ok {
		return
	}
	w.outSet[address] = struct{}{}

	hn := ref.Hostname()
	wu := WriterUrl{
		Loc:     address,
		LastMod: lastMod(lastModifiedHeader),
	}

	if _, ok := w.outMap[hn]; !ok {
		w.outMap[hn] = []WriterUrl{wu}
	} else {
		w.outMap[hn] = append(w.outMap[hn], wu)
	}

	lm := ""
	if wu.LastMod != nil {
		lm = *wu.LastMod
	}
	w.logger.Success(id, wu.Loc, lm)
}

func (w *Writer) Write() {
	if _, err := os.Stat(w.outDir); !os.IsNotExist(err) {
		if err = os.RemoveAll(w.outDir); err != nil {
			log.Fatalln(fmt.Errorf("failed to delete directory '%s' with error: %w", w.outDir, err))
		}
	}

	if err := os.Mkdir(w.outDir, 0755); err != nil {
		log.Fatalln(fmt.Errorf("failed to create directory '%s' with error: %w", w.outDir, err))
	}

	for hostname, urls := range w.outMap {
		if err := os.Mkdir(fmt.Sprintf("%s/%s", w.outDir, hostname), 0755); err != nil {
			log.Fatalln(fmt.Errorf("failed to create directory '%s/%s' with error: %w", w.outDir, hostname, err))
		}

		xmlFile, err := os.Create(fmt.Sprintf("%s/%s/%s", w.outDir, hostname, w.filename))
		if err != nil {
			log.Fatalln(fmt.Errorf("failed to create file '%s/%s/%s' with error: %w", w.outDir, hostname, w.filename, err))
		}
		if _, err = xmlFile.WriteString(xml.Header); err != nil {
			log.Fatalln(fmt.Errorf("failed to write to file '%s/%s/%s' with error: %w", w.outDir, hostname, w.filename, err))
		}

		urlSet := WriterUrlSet{
			XMLName: xml.Name{
				Space: w.xmlns,
				Local: "urlset",
			},
			Urls: urls,
		}

		encoder := xml.NewEncoder(xmlFile)
		encoder.Indent("", "  ")
		if err = encoder.Encode(&urlSet); err != nil {
			log.Fatalln("GetError encoding XML to file: ", err)
		}
	}
}

func lastMod(lmh string) *string {
	if lmh == "" {
		return nil
	}

	t, err := http.ParseTime(lmh)
	if err != nil {
		log.Print(fmt.Errorf("\nfailed to convert 'Last-Modified' header: %s, to time with error: %w\n", lmh, err))
		return nil
	}

	res := t.UTC().Format(time.RFC3339)[0:10]
	return &res
}
