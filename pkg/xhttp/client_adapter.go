package xhttp

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

// DefaultHTTPClientConfig is sets default http.Transport & http.Client configuration
// *** Go has well known implementation of unlimited RequestTimeout which is causing data leaks and system overload. ***
// *** It's mandatory to limit RequestTimeout to prevent such events.
var DefaultHTTPClientConfig = ClientConfig{
	MaxIdleConn:    100,
	RequestTimeout: 3,
	RequestHeaders: map[string][]string{
		"Accept":          {"text/html"},
		"Accept-Language": {"*"},
		"Cache-Control":   {"no-cache"},
		"User-Agent":      {"sitemap"},
	},
	LastModifiedHeaderName: "Last-Modified",
}

type (
	ClientConfig struct {
		MaxIdleConn            int
		RequestTimeout         int64
		RequestHeaders         map[string][]string
		LastModifiedHeaderName string
	}

	ClientAdapter struct {
		config *ClientConfig
		client *http.Client
	}
)

func NewClientAdapter() (*ClientAdapter, error) {
	return NewClientAdapterWithConfig(DefaultHTTPClientConfig)
}

func NewClientAdapterWithConfig(config ClientConfig) (*ClientAdapter, error) {
	transport := &http.Transport{
		MaxIdleConns:        config.MaxIdleConn,
		MaxIdleConnsPerHost: config.MaxIdleConn,
	}

	client := &http.Client{
		Timeout:   time.Duration(config.RequestTimeout) * time.Second,
		Transport: transport,
	}

	return NewClientAdapterWithClient(config, client)
}

// NewClientAdapterWithClient creates reusable client safe for
// concurrent use by multiple goroutines. From the documentation:
//
// The Client's Transport typically has internal state (cached TCP
// connections), so Clients should be reused instead of created as
// needed. Clients are safe for concurrent use by multiple goroutines.
//
// A Client is higher-level than a RoundTripper (such as Transport)
// and additionally handles HTTP details such as cookies and
// redirects.
func NewClientAdapterWithClient(config ClientConfig, client *http.Client) (*ClientAdapter, error) {
	return &ClientAdapter{
		config: &config,
		client: client,
	}, nil
}

func (c *ClientAdapter) GetWithContext(ctx context.Context, uri string) (*Response, error) {
	var err error

	_, err = url.Parse(uri)
	if err != nil {
		return nil, fmt.Errorf("invalid uri: %s with error: %w", uri, err)
	}

	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		return nil, err
	}

	for k, v := range c.config.RequestHeaders {
		req.Header[k] = v
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	var body []byte
	if res.StatusCode >= 200 && res.StatusCode < 300 {
		body, err = ioutil.ReadAll(res.Body)
	} else if res.StatusCode >= 300 {
		// http.Client is anyway following redirects, if any remains
		// unhandled I'll assume it's for the sake of simplicity
		// of this application
		err = NewHttpError(res.StatusCode, res.Status)
	}

	// From source (src/net/http/transfer.go):
	// Fully consume the body, which will also lead to us reading
	// the trailer headers after the body, if present.
	if err := res.Body.Close(); err != nil {
		// Overwriting possible error from ioutil.ReadAll(res.Body)
		// is done on purpose, because Close() will return more significant
		// error description
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	var r Response
	if lm := res.Header.Get(c.config.LastModifiedHeaderName); lm != "" {
		r.LastModified = lm
	}
	r.Url = res.Request.URL
	r.Body = body

	return &r, nil
}
