package xhttp

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"
)

type (
	Response struct {
		Url          *url.URL
		Body         []byte
		LastModified string
	}
)

func (r *Response) LastModifiedW3C() string {
	t, err := http.ParseTime(r.LastModified)
	if err != nil {
		log.Print(fmt.Errorf("\nfailed to convert 'Last-Modified' header: %s, to time with error: %w\n", r.LastModified, err))
		return ""
	}

	return t.UTC().Format(time.RFC3339)[0:10]
}
