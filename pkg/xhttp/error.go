package xhttp

type (
	HttpError struct {
		StatusCode int
		Status     string
	}
)

func NewHttpError(code int, status string) *HttpError {
	return &HttpError{
		StatusCode: code,
		Status:     status,
	}
}

func (e *HttpError) Error() string {
	return e.Status
}
