package xlog

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"
)

const (
	taskLogFilename    = "taskLog.txt"
	failLogFilename    = "failLog.txt"
	successLogFilename = "successLog.txt"
)

type Processor struct {
	taskFile      *os.File
	taskLogger    *log.Logger
	failedFile    *os.File
	failedLogger  *log.Logger
	successFile   *os.File
	successLogger *log.Logger
}

type (
	TaskLog struct {
		Time  time.Time `json:"time"`
		Id    int       `json:"id"`
		Url   string    `json:"url"`
		Depth int       `json:"depth"`
		Retry int       `json:"retry"`
	}

	FailLog struct {
		Time  time.Time `json:"time"`
		Id    int       `json:"id"`
		Url   string    `json:"url"`
		Retry int       `json:"retry"`
		Err   string    `json:"err"`
	}

	SuccessLog struct {
		Time    time.Time `json:"time"`
		Id      int       `json:"id"`
		Url     string    `json:"url"`
		LastMod string    `json:"date"`
	}
)

func New(dir string) *Processor {
	recreateDir(dir)
	tf := getFile(dir, taskLogFilename)
	fl := getFile(dir, failLogFilename)
	sl := getFile(dir, successLogFilename)

	return &Processor{
		taskFile:      tf,
		taskLogger:    log.New(tf, "TASK ", log.Lshortfile),
		failedFile:    fl,
		failedLogger:  log.New(fl, "FAIL ", log.Lshortfile),
		successFile:   sl,
		successLogger: log.New(sl, "SUCCESS ", log.Lshortfile),
	}
}

func (p *Processor) Stop() {
	if err := p.taskFile.Close(); err != nil {
		panic(err)
	}
	if err := p.failedFile.Close(); err != nil {
		panic(err)
	}
	if err := p.successFile.Close(); err != nil {
		panic(err)
	}
}

func (p *Processor) Task(id, depth, retry int, url string) {
	p.write(p.taskLogger, TaskLog{
		Time:  time.Now().UTC(),
		Id:    id,
		Url:   url,
		Depth: depth,
		Retry: retry,
	})
}

func (p *Processor) Failed(id, retry int, url string, err error) {
	p.write(p.failedLogger, FailLog{
		Time:  time.Now().UTC(),
		Id:    id,
		Url:   url,
		Retry: retry,
		Err:   err.Error(),
	})
}

func (p *Processor) Success(id int, url, lastMod string) {
	p.write(p.successLogger, SuccessLog{
		Time:    time.Now().UTC(),
		Id:      id,
		Url:     url,
		LastMod: lastMod,
	})
}

func (p *Processor) write(l *log.Logger, data interface{}) {
	var b []byte
	var err error

	if b, err = json.Marshal(data); err != nil {
		panic(err)
	}
	if _, err = l.Writer().Write(b); err != nil {
		panic(err)
	}
	if _, err = l.Writer().Write([]byte("\n")); err != nil {
		panic(err)
	}

	log.Println(l.Prefix(), string(b))
}

func recreateDir(dir string) {
	if _, err := os.Stat(dir); !os.IsNotExist(err) {
		if err = os.RemoveAll(dir); err != nil {
			panic(fmt.Errorf("failed to delete directory '%s' with error: %w", dir, err))
		}
	}

	if err := os.Mkdir(dir, 0755); err != nil {
		log.Fatalln(fmt.Errorf("failed to create directory '%s' with error: %w", dir, err))
	}
}

func getFile(dir, filename string) *os.File {
	path := fmt.Sprintf("%s/%s", dir, filename)
	if f, err := os.Create(path); err != nil {
		panic(fmt.Errorf("failed to create file for writing '%s' with error: %w", path, err))
	} else {
		return f
	}
}
